<?php

namespace App\Http\Controllers;

use App\Posts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**
         * Query all posts
         */
        $posts = Posts::all();

        /**
         * Query poge by page
         */
//        $current_post_post = Posts::paginate(Input::get('page'))->toArray();
//
//        $response_data = [
//            "data" => $current_post_post["data"],
//            "links" => [
//                "first" => $current_post_post["first_page_url"],
//                "last" => $current_post_post["last_page_url"],
//                "prev" => $current_post_post["prev_page_url"],
//                "next" => $current_post_post["next_page_url"],
//            ],
//            "meta" => [
//                "current_page" => $current_post_post["current_page"],
//                "from" => $current_post_post["from"],
//                "last_page" => $current_post_post["last_page"],
//                "path" => $current_post_post["path"],
//                "per_page" => $current_post_post["per_page"],
//                "to" => $current_post_post["to"],
//                "total" => $current_post_post["total"],
//            ]
//        ];

        return response()->json([
            "data" => $posts
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = Posts::create([
            "title" => $request->title,
            "image" => $request->image,
            "content" => $request->get('content'),
            "slug" => str_slug($request->title),
            "user_id" => $request->user()->id
        ]);

        return response()->json([
            "data" => $post
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = Posts::where('slug', $slug)->first();

        return response()->json( [
            "data" => $post
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $post = Posts::where('slug', $slug)->first();
        $post->title = $request->title;
        $post->image = $request->image;
        $post->content = $request->get('content');
        $post->slug = str_slug($request->title);
        $post->save();

        return response()->json([
            "data" => $post
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $post = Posts::where('slug', $slug)->first();
        $post->delete();

        if ($post->trashed()) {
            return response()->json([
                "status" => "record deleted successfully"
            ]);
        }
    }
}
