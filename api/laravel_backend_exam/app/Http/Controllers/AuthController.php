<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginValidation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Authenticate user login
     *
     * @param LoginValidation $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate(LoginValidation $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            $user = $request->user();

            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;

            if ($request->remember_me)
                $token->expires_at = Carbon::now()->addWeeks(1);

            $token->save();

            $user_response = [
                "user" => $request->user(),
                "token" => $tokenResult->accessToken,
                "token_type" => "Bearer",
                "expires_at" => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString()
            ];

            return response()->json($user_response);
        }
        else
        {
            // Authentication failed...
            return response()->json("Invalid username or password.");
        }
    }

    /**
     * Logout User
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
//        $request->user()->token()->revoke();

        return response()->json($request->user());
    }

    /**
     * Retrieve logged user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }
}
