<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Posts;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Get list of comments in specified slug
     *
     * @param $slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($slug)
    {
        $post = Posts::where('slug', $slug)->first();

        return response()->json([
            "data" => $post->comments
        ]);
    }

    /**
     * Save post comments in specified slug
     *
     * @param Request $request
     * @param $slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $slug)
    {
        $post = Posts::where('slug', $slug)->first();

        $comment = new Comments([
            'body' => $request->body,
            'creator_id' => $post->user->id,
            'creator_type' => get_class($post->user),
        ]);

        $post->comments()->save($comment);

        return response()->json($post->comments);
    }

    /**
     * Updates specific comment
     *
     * @param Request $request
     * @param $slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $comment = Comments::find($id);

        $comment->update([
            "body" => $request->body
        ]);

        return response()->json($comment);
    }

    public function destroy($slug, $id)
    {
        $post = Posts::where('slug', $slug)->first();
        $post->comments()->where('id', $id)->delete();

        return response()->json([
            "status" => "record deleted successfully"
        ]);
    }
}
