<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterValidation;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Register new user and encrypt password
     *
     * @param RegisterValidation $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(RegisterValidation $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return response()->json($user);
    }
}
