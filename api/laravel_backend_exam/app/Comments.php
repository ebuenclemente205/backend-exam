<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comments extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'body',
        'creator_id',
        'creator_type',
        'commentable_id',
        'commentable_type',
        'parent_id'
    ];

    /**
     * Get all of the models that own comments.
     */
    public function comment()
    {
        return $this->morphTo();
    }
}
