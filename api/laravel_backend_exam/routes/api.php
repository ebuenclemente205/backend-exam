<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'AuthController@authenticate')->name('login');
Route::post('/register', 'usercontroller@store')->name('register');

Route::get('/posts/{slug}', 'PostController@show')->name('post-show');
Route::get('/posts/{slug}/comments', 'CommentController@index')->name('comment-index');
Route::get('/posts', 'PostController@index')->name('post-index');

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('/logout', 'AuthController@logout')->name('logout');
    Route::get('/user', 'AuthController@user')->name('user');

    Route::post('/posts', 'PostController@store')->name('post-store');
    Route::patch('/posts/{slug}', 'PostController@update')->name('post-update');
    Route::delete('/posts/{slug}', 'PostController@destroy')->name('post-delete');

    Route::post('/posts/{slug}/comments', 'CommentController@store')->name('comment-store');
    Route::patch('/posts/{slug}/comments/{id}', 'CommentController@update')->name('comment-update');
    Route::delete('/posts/{slug}/comments/{id}', 'CommentController@destroy')->name('comment-destroy');
});
